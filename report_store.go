package main

import (
	"time"

	"github.com/jackc/pgx"
)

const (
	stmtInsertReport     = "insert_report"
	stmtHasReport        = "has_report"
	stmtFindLatestForURL = "find_latest_for_url"
	stmtReportCount      = "report_count"
)

type reportStore struct {
	pool *pgx.ConnPool
}

type dbReport struct {
	ID      int32
	Report  report
	URL     string
	Created time.Time
}

func createReportStore(pgpool *pgx.ConnPool) (*reportStore, error) {
	s := &reportStore{
		pool: pgpool,
	}

	if _, err := s.pool.Prepare(stmtInsertReport, `
INSERT INTO ecometer.reports (url, report, sys_usage, job_creation_timestamp, job_start_timestamp) VALUES ($1, $2, $3, $4, $5)
`); err != nil {
		return nil, err
	}
	if _, err := s.pool.Prepare(stmtHasReport, `
SELECT EXISTS (SELECT 1 FROM ecometer.reports WHERE url = $1)
`); err != nil {
		return nil, err
	}
	if _, err := s.pool.Prepare(stmtFindLatestForURL, `
SELECT id, report, url, creation_timestamp
FROM ecometer.reports
WHERE url = $1
ORDER BY creation_timestamp DESC
LIMIT 1
`); err != nil {
		return nil, err
	}
	if _, err := s.pool.Prepare(stmtReportCount, `
SELECT json_build_object(
	'status',              json_object_agg(name, count),
	'total',               SUM(count),
	'total_job_wait_time', SUM(job_wait_time),
	'total_job_duration',  SUM(job_duration)
)
FROM (
	SELECT
		COALESCE(report->'error'->>'name', 'SUCCESS') AS name,
		COUNT(*) AS count,
		SUM(EXTRACT('epoch' FROM job_start_timestamp - job_creation_timestamp)) AS job_wait_time,
		SUM(EXTRACT('epoch' FROM creation_timestamp - job_start_timestamp)) AS job_duration
	FROM ecometer.reports
	GROUP BY name
) AS stats
`); err != nil {
		return nil, err
	}

	return s, nil
}

func (s *reportStore) Insert(j job, report *report, sysUsage interface{}) error {
	_, err := s.pool.Exec(stmtInsertReport, j.URL, report, sysUsage, j.Created, j.Started)
	return err
}

func (s *reportStore) HasReport(url string) (has bool, err error) {
	err = s.pool.QueryRow(stmtHasReport, url).Scan(&has)
	return
}

func (s *reportStore) FindLatestForURL(url string) (*dbReport, error) {
	r := new(dbReport)
	err := s.pool.QueryRow(stmtFindLatestForURL, url).Scan(&r.ID, &r.Report, &r.URL, &r.Created)
	if err == pgx.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return r, nil
}

func (s *reportStore) Stats() (json []byte, err error) {
	err = s.pool.QueryRow(stmtReportCount).Scan(&json)
	return
}
