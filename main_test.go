package main

import (
	"compress/gzip"
	"net/http/httptest"
	"testing"
)

func TestCompress(t *testing.T) {
	testdata := []struct {
		acceptEncoding          string
		expectedContentEncoding string
	}{
		{"", ""},
		{"*", ""},
		{"identity", ""},
		{"deflate", ""},
		{"br", ""},
		{"gzip", "gzip"},
		{"foogzip", ""},
		{"gzipbar", ""},
		{"foogzipbar", ""},
		{"x-gzip", ""},
		{"gzip,deflate", "gzip"},
		{"gzip, deflate", "gzip"},
		{"deflate,gzip", "gzip"},
		{"deflate; gzip", "gzip"},
		{"gzip;q=0.5, deflate;q=0.8", "gzip"},
		{"deflate;q=0.8, gzip;q=0.5", "gzip"},
	}
	for _, d := range testdata {
		req := httptest.NewRequest("GET", "/whatever", nil)
		req.Header.Set("Accept-Encoding", d.acceptEncoding)
		rw := httptest.NewRecorder()

		w := compress(rw, req)

		if actualContentEncoding := rw.Result().Header.Get("Content-Encoding"); actualContentEncoding != d.expectedContentEncoding {
			t.Errorf("compress: 'Accept-Encoding: %s', expected %s, got %s",
				d.acceptEncoding, d.expectedContentEncoding, actualContentEncoding)
		}

		switch d.expectedContentEncoding {
		case "":
			if w != rw {
				t.Errorf("compress: expected writer to be the http.ResponseWriter, got %T", w)
			}
		case "gzip":
			if _, ok := w.(*gzip.Writer); !ok {
				t.Errorf("compress: expected gzip.Writer, got %T", w)
			}
		}
	}
}

func TestEllipsis(t *testing.T) {
	testdata := []struct {
		text           string
		expectedResult string
		maxLength      int
	}{
		{"textLengthAboveMaxLength", "tex…gth", 10},
		{"textLengthBelowMaxLength", "textLengthBelowMaxLength", 100},
		{"textLengthEqualToMaxLength", "textLengthEqualToMaxLength", 26},
		{"textLengthEqualToMaxLengthOdd", "textLengthEqualToMaxLengthOdd", 29},
		{"textLengthEqualToMaxLengthPlusOne", "textLengthEqua…axLengthPlusOne", 32},
		{"textLengthEqualToMaxLengthPlusOneOdd", "textLengthEqualT…LengthPlusOneOdd", 35},
	}
	for _, d := range testdata {
		result := ellipsis(d.text, d.maxLength)
		// fmt.Printf("max: %v : %s (%v) ->  %s (%v)\n", d.maxLength, d.text, len(d.text), result, len(result))
		if len(result) > d.maxLength {
			t.Errorf("ellipsis: len('%s') should be less than or equal to %v, got %v", result, d.maxLength, len(result))
		}
		if result != d.expectedResult {
			t.Errorf("ellipsis: expected '%s', got '%s'", d.expectedResult, result)
		}
	}
}
