package main

import (
	"bytes"
	"flag"
	"fmt"
	"go/format"
	htemplate "html/template"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	ttemplate "text/template"

	"github.com/russross/blackfriday"
	"gopkg.in/yaml.v2"
)

type ruleDescription struct {
	ID          int            `yaml:"id"`
	Slug        string         `yaml:"slug"`
	Title       string         `yaml:"title"`
	Context     string         `yaml:"context"`
	Summary     htemplate.HTML `yaml:"summary"`
	Description htemplate.HTML `yaml:"description"`
	Example     htemplate.HTML `yaml:"example"`
	Template    htemplate.HTML `yaml:"template"`

	yamlPath string
}

var (
	input   = flag.String("yaml", "rules/", "Folder containing YAML rule descriptions")
	tmpls   = flag.String("tmpl", "views/", "Folder containing HTML templates")
	scripts = flag.String("js", "webpage-analyzer/rules/", "Folder containing the rules check scripts")
	htmlOut = flag.String("html", "static/rules/", "Output folder for HTML-rendered rule descriptions")
	goOut   = flag.String("go", "rules_data.go", "Output file for Go-generate source")
)

var goTmpl = ttemplate.Must(ttemplate.New("rules_data.go").Parse(`package main
import "html/template"

var rules = [...]rule{
{{- range . }}
	{
		ID:       {{printf "%#v" .ID}},
		Slug:     {{printf "%#v" .Slug}},
		Title:    {{printf "%#v" .Title}},
		Context:  {{printf "context%s" .Context}},
		Summary:  {{printf "%#v" .Summary}},
	},
{{- end}}
}

var rulesBySlug = map[string]*rule{
{{- range $i, $rule := . }}
	{{printf "%#v" $rule.Slug}}: &rules[{{printf "%#v" $i}}],
{{- end}}
}

func init() {
{{- range .}}
	template.Must(views.New({{printf "%#v" .Slug}}).Parse({{printf "%#v" .Template}}))
{{- end}}
}
`))

var (
	ruleHTMLTmpl     *htemplate.Template
	ruleListHTMLTmpl *htemplate.Template

	rulesByID   = make(map[int]*ruleDescription)
	rulesBySlug = make(map[string]*ruleDescription)
)

func main() {
	log.SetFlags(0)

	flag.Parse()

	ruleHTMLTmpl = htemplate.Must(htemplate.ParseFiles(filepath.Join(*tmpls, "rule.html")))
	ruleListHTMLTmpl = htemplate.Must(htemplate.ParseFiles(filepath.Join(*tmpls, "rule_list.html")))

	if err := os.MkdirAll(*htmlOut, 0777); /* #nosec */ err != nil {
		log.Fatal(err)
	}

	var allRules []*ruleDescription
	if autoRules, err := processAllYamls("auto"); err != nil {
		log.Fatal(err)
	} else {
		if err := checkAutoRules(autoRules); err != nil {
			log.Fatal(err)
		}
		allRules = append(allRules, autoRules...)
	}

	if formRules, err := processAllYamls("form"); err != nil {
		log.Fatal(err)
	} else {
		allRules = append(allRules, formRules...)
		// TODO: generate HTML form
	}

	if otherRules, err := processAllYamls("other"); err != nil {
		log.Fatal(err)
	} else {
		allRules = append(allRules, otherRules...)
	}

	sortById(allRules)

	if err := generateGo(allRules); err != nil {
		log.Fatal(err)
	}
	if err := generateHTMLList(allRules); err != nil {
		log.Fatal(err)
	}
}

func processAllYamls(subfolder string) (rules []*ruleDescription, err error) {
	ymls, err := filepath.Glob(filepath.Join(*input, subfolder, "*.yml"))
	if err != nil {
		return
	}
	for _, yml := range ymls {
		var r *ruleDescription
		r, err = processYaml(yml)
		if err != nil {
			return
		}
		rules = append(rules, r)
	}
	return
}

func processYaml(yml string) (r *ruleDescription, err error) {
	b, err := ioutil.ReadFile(yml)
	if err != nil {
		return
	}
	r = &ruleDescription{yamlPath: yml}
	if err = yaml.Unmarshal(b, r); err != nil {
		return
	}

	checkRule(r)

	r.processMarkdown()

	f, err := os.Create(filepath.Join(*htmlOut, r.Slug+".html"))
	if err != nil {
		return
	}
	defer f.Close() // #nosec
	return r, ruleHTMLTmpl.Execute(f, r)
}

func (r *ruleDescription) Filename() string {
	return fmt.Sprintf("%d-%s", r.ID, r.Slug)
}

func (r *ruleDescription) processMarkdown() {
	processMarkdown(&r.Summary)
	processMarkdown(&r.Description)
	processMarkdown(&r.Example)
	processMarkdown(&r.Template)
}

func processMarkdown(h *htemplate.HTML) {
	if *h == "" {
		return
	}
	*h = htemplate.HTML(blackfriday.MarkdownCommon([]byte(*h))) // #nosec
}

func checkAutoRules(autoRules []*ruleDescription) error {
	for _, r := range autoRules {
		if r.Template == "" {
			log.Printf("%s:Missing a 'template' field\n", r.yamlPath)
		}

		scriptFilename := filepath.Join(*scripts, r.Filename()+".js")
		fi, err := os.Stat(scriptFilename)
		if os.IsNotExist(err) {
			log.Printf("%s:No corresponding script %q", r.yamlPath, scriptFilename)
			continue
		} else if err != nil {
			return err
		}
		if !fi.Mode().IsRegular() {
			log.Printf("%s:Corresponding script %q is not a regular file", r.yamlPath, scriptFilename)
		}
	}

	scriptNames, err := filepath.Glob(filepath.Join(*scripts, "*.js"))
	if err != nil {
		return err
	}
outer:
	for _, sn := range scriptNames {
		if filepath.Base(sn) == "index.js" {
			continue
		}
		fn := filepath.Base(sn)
		for _, r := range autoRules {
			if r.Filename()+".js" == fn {
				continue outer
			}
		}
		log.Printf("%s:No corresponding auto rule", sn)
	}
	return nil
}

func checkRule(r *ruleDescription) {
	if strings.HasPrefix(r.Slug, strconv.Itoa(r.ID)+"-") {
		log.Printf("%s:Slug %q should not begin with its ID (%d)\n", r.yamlPath, r.Slug, r.ID)
		r.Slug = r.Slug[strings.IndexByte(r.Slug, '-')+1:]
	}
	if g, e := filepath.Base(r.yamlPath), r.Filename()+".yml"; g != e {
		log.Printf("%s:Should be named %q\n", r.yamlPath, e)
	}

	if d, ok := rulesByID[r.ID]; ok {
		log.Printf("%s:Duplicate ID with rule %q", r.yamlPath, d.yamlPath)
	} else {
		rulesByID[r.ID] = r
	}
	if d, ok := rulesBySlug[r.Slug]; ok {
		log.Printf("%s:Duplicate slug with rule %q", r.yamlPath, d.yamlPath)
	} else {
		rulesBySlug[r.Slug] = r
	}

	if r.Slug == "index" {
		log.Printf("%s:Slug should not be %q", r.yamlPath, r.Slug)
	}
	switch r.Context {
	case "Design", "Development", "Hosting":
	case "":
		log.Printf("%s:Missing context", r.yamlPath)
		r.Context = "Unknown"
	default:
		log.Printf("%s:Unknown context value %q", r.yamlPath, r.Context)
	}
}

func sortById(rules []*ruleDescription) {
	sort.Slice(rules, func(i, j int) bool { return rules[i].ID < rules[j].ID })
}

func generateGo(allRules []*ruleDescription) error {
	var buf bytes.Buffer
	if err := goTmpl.Execute(&buf, allRules); err != nil {
		return err
	}
	b, err := format.Source(buf.Bytes())
	if err != nil {
		return err
	}
	f, err := os.Create(*goOut)
	if err != nil {
		return err
	}
	defer f.Close() // #nosec
	_, err = f.Write(b)
	return err
}

func generateHTMLList(allRules []*ruleDescription) error {
	f, err := os.Create(filepath.Join(*htmlOut, "index.html"))
	if err != nil {
		return err
	}
	defer f.Close() // #nosec
	return ruleListHTMLTmpl.Execute(f, allRules)
}
