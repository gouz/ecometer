function checkStatus() {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);
        if (response.status) {
          window.location.replace('/report' + window.location.search);
          return;
        }
      }
      window.setTimeout(checkStatus, 2000);
    }
  };
  xhr.open('GET', '/ajax/job' + window.location.search, true);
  xhr.setRequestHeader('Cache-Control', 'no-cache');
  xhr.send();
}
window.setTimeout(checkStatus, 2000);
