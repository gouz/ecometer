// Copyright (C) 2016  The EcoMeter authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"log"
	"time"
)

func dispatcher(done <-chan struct{}, numWorkers int, analyzr *analyzer) {
	workers := make(chan func(j job) error, numWorkers)
	for i := 0; i < numWorkers; i++ {
		workers <- func(j job) error {
			r, s, err := analyzr.Analyze(j.URL)
			if err != nil {
				return err
			}
			return store.Insert(j, r, s)
		}
	}
	logError := func(u string, err error) {
		log.Printf("Error during analysis of %s: %v\n", u, err)
	}
outerLoop:
	for {
		select {
		case w := <-workers:
			j, err := jobs.Take()
			for err != nil {
				// try again later
				select {
				case <-time.After(5 * time.Second):
					j, err = jobs.Take()
				case <-done:
					break outerLoop
				}
			}
			go func(j job) {
				if err := w(j); err == nil {
					if err = jobs.MarkCompleted(j.URL); err != nil {
						logError(j.URL, err)
					}
				} else {
					logError(j.URL, err)
					if err = jobs.MarkFailed(j.URL); err != nil {
						logError(j.URL, err)
					}
				}
				workers <- w
			}(j)
		case <-done:
			break outerLoop
		}
	}
}
