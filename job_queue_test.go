// +build database

package main

import (
	"flag"
	"testing"
)

var dsn = flag.String("dsn", "", "Data source name of the PostgreSQL database")

func TestJobQueue(t *testing.T) {
	if !flag.Parsed() {
		flag.Parse()
	}

	pgpool, err := createPGConnPool(*dsn)
	if err != nil {
		t.Fatal(err)
	}

	jobs, err := createJobQueue(pgpool)
	if err != nil {
		t.Fatal(err)
	}

	jobs.clear()
	defer jobs.clear()

	if _, ok := jobs.Get("http://example.net/"); ok {
		t.Error("Found job in empty queue!")
	}

	j, err := jobs.Add("http://example.net/")
	if err != nil {
		t.Fatal(err)
	}
	if j.URL != "http://example.net/" {
		t.Errorf("jobQueue.add: job.URL: got %s, want %s", j.URL, "http://example.net/")
	}
	if j.Created.IsZero() {
		t.Error("jobQueue.add: job.Created: got zero, want now")
	}
	if !j.Started.IsZero() {
		t.Errorf("jobQueue.add: job.Started: got %v, want zero", j.Started)
	}

	j, ok := jobs.Get("http://example.net/")
	if !ok {
		t.Errorf("jobQueue.get: got no job, expected one")
	}
	if j.URL != "http://example.net/" {
		t.Errorf("jobQueue.get: job.URL: got %s, want %s", j.URL, "http://example.net/")
	}
	if j.Created.IsZero() {
		t.Error("jobQueue.get: job.Created: got zero, want now")
	}
	if !j.Started.IsZero() {
		t.Errorf("jobQueue.get: job.Started: got %v, want zero", j.Started)
	}

	// if _, err = jobs.Add("http://another.example.net"); err == nil {
	// 	t.Errorf("jobQueue.add: expected error (job queue full), got none")
	// }

	if j2, err2 := jobs.Add("http://example.net/"); err2 != nil {
		t.Error(err2)
	} else if j2 != j {
		t.Errorf("jobQueue.add: expected reuse of a previous job %v, got new job %v", j2, j)
	}

	j, err = jobs.Take()
	if err != nil {
		t.Error("jobQueue.take: got no job, expected one", err)
	}
	if j.URL != "http://example.net/" {
		t.Errorf("jobQueue.take: job.URL: got %s, want %s", j.URL, "http://example.net/")
	}
	if j.Created.IsZero() {
		t.Error("jobQueue.take: job.Created: got zero, want now")
	}
	if j.Started.IsZero() {
		t.Errorf("jobQueue.take: job.Started: got %v, want now", j.Started)
	}

	j, ok = jobs.Get("http://example.net/")
	if !ok {
		t.Errorf("jobQueue.get: got no job, expected one")
	}
	if j.URL != "http://example.net/" {
		t.Errorf("jobQueue.get: job.URL: got %s, want %s", j.URL, "http://example.net/")
	}
	if j.Created.IsZero() {
		t.Error("jobQueue.get: job.Created: got zero, want now")
	}
	if j.Started.IsZero() {
		t.Errorf("jobQueue.get: job.Started: got %v, want now", j.Started)
	}

	if j2, err2 := jobs.Add("http://example.net/"); err2 != nil {
		t.Error(err2)
	} else if j2 != j {
		t.Errorf("jobQueue.add: expected reuse of a previous job %v, got new job %v", j2, j)
	}

	if err = jobs.MarkCompleted("http://example.net/"); err != nil {
		t.Error(err)
	}

	if _, ok = jobs.Get("http://example.net/"); ok {
		t.Errorf("jobQueue.get: got job, expected none")
	}
}
