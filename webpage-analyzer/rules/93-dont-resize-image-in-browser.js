import { OK, NOK, NA } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/18
export const name = '93-dont-resize-image-in-browser';
export function check(page) {
  const data = page.evaluate(function() {
    // TODO: handle background images resized with background-size.
    // TODO: detect "responsive design" where the image's size depend on the
    // viewport dimensions, but is not sized-down passed a given threshold.
    var images =Array.from(document.getElementsByTagName('img'));
    var data = {
      totalImages : images.length,
      resizedImages: images.filter(function(img) {
        return img.clientWidth < img.naturalWidth
                || img.clientHeight < img.naturalHeight;
      }).map(img => {
        return {
          src: img.src,
          naturalWidth: img.naturalWidth,
          naturalHeight: img.naturalHeight,
          displayWidth: img.clientWidth,
          displayHeight: img.clientHeight,
        };
      }),
    };
    return data;
  });

  const status = data.totalImages.length === 0 ? NA : ( data.resizedImages.length === 0 ? OK : NOK );

  return {
    slug: 'dont-resize-image-in-browser',
    id: 93,
    status: status,
    data: data,
  };

}

export default { name, check };
