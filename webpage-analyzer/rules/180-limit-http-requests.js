import { OK, NOK } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/37
export const name = '180-limit-http-requests';
export function check(page, resources) {

  // From http://httparchive.org/trends.php#bytesTotal&reqTotal
  // TODO refine this threshold and the "magic" reducer
  const totalRequestsAverage2016 = 106;
  const ratio = 25;
  const goal = Math.floor(ratio/100*totalRequestsAverage2016);
  const totalRequests = Object.keys(resources).length;

  // FTR, tbroyer said:
  //
  // Should we try to factor in redirects here? (there's another rule about
  // avoiding redirects, so we could ignore them here; most people think in terms
  // of "assets" or "initial requests" I believe, rather than pure "requests").
  //
  // Imagine a site with few "initial requests" except for one that does a lot of
  // redirects (enough to cross the threshold). Should the page fail the 2 rules
  // (too many requests + too many redirects), or should it only fail the "avoid
  // redirects" rule?

  return {
    id: 180,
    slug: 'limit-http-requests',
    status: totalRequests <= goal ? OK : NOK,
    data: { totalRequests, totalRequestsAverage2016, ratio, goal },
  };

}

export default { name, check };
