import { forEachFrame } from '../helpers.js';
import { OK, NOK } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/31
export const name = '10-externalize-css-and-js';
export function check(page) {

  const frames = [];

  forEachFrame(page, () => {
    frames.push({
      url: page.frameUrl,
      inlinedStylesheets: page.evaluate(function() {
        return Array.from(document.styleSheets)
          .filter(s => !s.href).length;
      }),
      inlinedScripts: page.evaluate(function() {
        // JSON+LD scripts cannot be externalized
        return Array.from(document.scripts)
          .filter(s => s.type !== 'application/ld+json' && !s.src).length;
      }),
    });
  });

  // We should have no inlined scripts and stylesheets but we allow 1 potential inlined JS to
  // handle global configuration
  // TODO refine this
  // Should we had a "content size" threshold ?
  // Should we be more forgiving ? What should be the goal ?
  const jsGoal = 1;
  const cssGoal = 0;

  return {
    slug: 'externalize-css-and-js',
    id: 7,
    status: frames.every(f => f.inlinedStylesheets <= cssGoal && f.inlinedScripts <= jsGoal) ? OK : NOK,
    data: { frames, jsGoal, cssGoal },
  };

}

export default { name, check };
