import system from 'system';
import { asset,  compressible } from './mime';

const DEBUG = system.env.DEBUG === 'yes';

/**
 * Print message to stderr only if DEBUG variable is truthy
 */
export function debug(lazyString) {
  if (!DEBUG) return;
  const message = typeof lazyString === 'function' ? lazyString() : lazyString;
  system.stderr.write(`[DEBUG] ${message}\n`);
}

/**
 * Return true is the given mime type is considered to be an asset (static resource)
 */
export function isAsset(mimeType) {
  return asset.some(value => value.test(mimeType));
}

/**
 * Return true is the given mime type is considered to be a compressible resource
 */
export function isCompressible(mimeType) {
  return compressible.some(value => value.test(mimeType));
}

/**
 * Return true if the given resource was downloaded successfully
 */
export function isResourceCompleted(resource) {
  if (!resource) return false;
  return !resource.error &&
    resource.res &&
    resource.res.status >= 200 && resource.res.status < 400
  ;
}

/**
 * Execute a given function for each frame present in page
 */
export function forEachFrame(page, cb, resetToMainFrame = true) {

  if (resetToMainFrame) {
    debug('Resetting to main frame...');
    page.switchToMainFrame();
  }

  // Execute callback for the current frame
  cb(page);

  // Iterate over child frames
  for (let i = 0; i < page.framesCount; i++) {
    page.switchToFrame(i);
    debug(() => `Switching to frame "${page.frameName || '#'+i}" (${page.frameUrl})...`);
    forEachFrame(page, cb, false);
    page.switchToParentFrame();
  }

}

/**
 * Find a resource in the given "collection" via its url
 * Will follow HTTP redirection by default
 */
export function findResourceByUrl(resources, url, followRedirect = true) {

  debug(() => `Searching for resource ${url}`);

  for (let resId of Object.keys(resources)) {

    let resource = resources[resId];
    let response = resource.res;
    let request = resource.req;

    // If their is no request attached or its url does not match ours, skip to next resource
    if (!request || request.url !== url) continue;

    // if we follow redirection and there is a redirect response, find the associated resource
    if (followRedirect && response && response.redirectURL) {
      debug(() => `Following resource redirect ${url} -> ${response.redirectURL}`);
      resource = findResourceByUrl(resources, response.redirectURL, true);
    }

    return resource;

  }

}

/**
 * Count character occurences in the given string
 */
export function countChar(char, str) {
  let total = 0;
  for (let curr, i = 0; (curr = str[i]); i++) {
    if (curr === char) total++;
  }
  return total;
}

/**
 * Detect minification for Javascript and CSS files
 */
export function isMinified(scriptContent) {

  if (!scriptContent) return true;

  const total = scriptContent.length-1;
  const semicolons = countChar(';', scriptContent);
  const linebreaks = countChar('\n', scriptContent);

  // Empiric method to detect minified files
  //
  // javascript code is minified if, on average:
  //  - there is more than one semicolon by line
  //  - and there are more than 100 characters by line
  const isMinified = semicolons/linebreaks > 1 && linebreaks/total < 0.01;

  debug(() => `S/L ${(semicolons/linebreaks).toFixed(2)} L/T ${(linebreaks/total).toFixed(2)} Minified: ${isMinified}`);

  return isMinified;

}
