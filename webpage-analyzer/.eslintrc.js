/* eslint import/no-commonjs:off */
/* eslint-env node */

module.exports = {
  'env': {
    'phantomjs': true,
    'es6': true,
  },
  'plugins': [
    'compat',
  ],
  'rules': {
    // compat plugin
    'compat/compat': 'error',
  },
  'settings': {
    'browsers': [ 'Firefox 59' ],
    // SlimerJS core modules; 'fs' and 'child_process' are also Node core modules so need not be redefined here
    'import/core-modules': [ 'system', 'webpage', 'webserver' ],
  },
};
