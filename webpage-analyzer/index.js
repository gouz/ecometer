// Require modules
import system from 'system';
import webpage from 'webpage';
import { debug } from './helpers.js';
import rules from './rules/index.js';

// Create global variables

const NETWORK_ACTIVITY_WAIT = 5000;
const PAGE_READY_TIMEOUT = 30000;

const page = webpage.create();
const pageURL = system.args[1];
const resources = {};
const report = {
  timestamp: new Date(),
  rules: [],
};
// Errors
const errors = {
  INVALID_ARGUMENT: 'INVALID_ARGUMENT',
  PAGE_LOAD_FAILED: 'PAGE_LOAD_FAILED',
  RULE_ERROR: 'RULE_ERROR',
};

// Flag preventing the output of additional information (error or report) on stdout
// after exiting (used by printErrorAndExit and printReportAndExit, must be declared
// before any call to those methods)
let exiting = false;

// Assert arguments
if (!pageURL) {
  printErrorAndExit(errors.INVALID_ARGUMENT, 'You must provide a valid URL as first argument.');
} else {
  // Configure page settings

  // TODO: Handle Basic Auth for private websites ?
  // page.settings.userName = '...';
  // page.settings.password = '...';
  page.settings.resourceTimeout = 10000;

  // TODO Make viewport and user agent configurable
  // page.settings.userAgent = '...';
  // XXX: viewportSize is set to the wanted size of the screenshot,
  // and zoomFactor such that the page is rendered as if the viewport was 1280×864
  // XXX: keep screenshot size in sync with the report.html view's <img width= height=>.
  page.viewportSize = {
    width: 320,
    height: 216,
  };
  page.clipRect = {
    top: 0,
    left: 0,
    width: page.viewportSize.width,
    height: page.viewportSize.height,
  };
  page.zoomFactor = page.viewportSize.width / 1280;

  page.captureContent = [ /.*/ ];

  page.onError = function(msg) {
    debug(() => `Page error: ${msg}`);
  };

  const pageReadyTimer = whenPageReady(NETWORK_ACTIVITY_WAIT, PAGE_READY_TIMEOUT, err => {
    if (err) return printErrorAndExit(err.name, err.message);

    debug(() => `Total resources loaded: ${Object.keys(resources).length}`);

    debug('Analyzing page...');

    if (system.env.NO_SCREENSHOT !== 'yes') takeScreenshot();

    for (let rule, i = 0; (rule = rules[i]); i++) {
      try {
        debug(() => `Checking rule "${rule.name}"`);
        const result = rule.check(page, resources);
        if (result) report.rules.push(result);
      } catch (err) {
        return printErrorAndExit(errors.RULE_ERROR, `Error while executing rule "${rule.name}": ${err.message} ${err.stack}`);
      }
    }

    return printReportAndExit();

  });

  page.onResourceRequested = function(req) {
    debug(() => `Resource requested "${req.url}"`);
    resources[req.id] = {
      downloadStart: req.time,
      req: req,
      res: null,
      timeout: null,
      error: null,
    };
    pageReadyTimer.clear();
  };

  page.onResourceTimeout = function(req) {
    debug(() => `Resource loading timeout for "${req.url}"`);
    if (matchPageURL(req.url)) {
      return printErrorAndExit(
        errors.PAGE_LOAD_FAILED,
        `Couldn't load URL. Error: "${req.errorString}"'`
      );
    }
    resources[req.id]['timeout'] = req;
    pageReadyTimer.start();
    // TODO Handle resources loading failure
  };

  page.onResourceError = function(req) {
    debug(() => `Couldn't load resource "${req.url}"`);
    if (matchPageURL(req.url)) {
      return printErrorAndExit(
        errors.PAGE_LOAD_FAILED,
        `Couldn't load URL. Error: "${req.errorString}"`
      );
    }
    resources[req.id]['error'] = req;
    pageReadyTimer.start();
  };

  page.onResourceReceived = function(res) {
    const resource = resources[res.id];
    if (res.stage !== 'end') return;
    debug(() => `Resource loaded "${res.url}"`);
    resource.res = res;
    pageReadyTimer.start();
  };

  page.onNavigationRequested = function(url) {
    debug(() => `Navigation requested to "${url}"`);
    pageReadyTimer.clear();
  };

  // Bind event handlers
  page.onLoadFinished = pageReadyTimer;

  // Load web page
  debug(() => `Loading page "${pageURL}"...`);

  page.open(pageURL);
}
// ------------------ Methods ------------------

/**
 * Return a callback for page onLoadFinished event with an
 * additional wait of <networkInactivityWait> milliseconds after
 * the last network activity detected OR a <pageReadyTimeout> milliseconds
 * timeout.
 * Will call <done> on finish.
 */
function whenPageReady(networkInactivityWait, pageReadyTimeout, done) {

  let pageReadyTimeoutId = null;
  let pageLoadFinished = false;
  let pageReady = false;

  const pageReadyTimer = status => {

    debug(() => `onPageLoadFinished triggered. Response status: "${status}"`);

    if (status !== 'success') {
      return done({
        name: errors.PAGE_LOAD_FAILED,
        message: `Couldn't load page, status: "${status}"`,
      });
    }

    pageLoadFinished = true;

    pageReadyTimer.start();

    setTimeout(() => {
      debug(() => `Page ready timed out after ${pageReadyTimeout}ms`);
      pageReadyTimer.clear();
      pageReady = true;
      done();
    }, pageReadyTimeout);

  };

  pageReadyTimer.start = () => {
    if (pageReady || !pageLoadFinished) return;
    pageReadyTimer.clear();
    debug(() => `Restart page ready timer ${networkInactivityWait}ms`);
    pageReadyTimeoutId = setTimeout(function() {
      pageReady = true;
      done();
    }, networkInactivityWait);
  };

  pageReadyTimer.clear = () => {
    if (pageReady || !pageLoadFinished) return;
    debug('Clearing page ready timer');
    clearTimeout(pageReadyTimeoutId);
    pageReadyTimeoutId = null;
  };

  return pageReadyTimer;

}

/**
 * Format/print report as JSON and exit PhantomJS with exit status 0
 */
function printReportAndExit() {
  if (exiting) return;
  debug('Printing report...');
  // TODO Better error handling/normalization
  report.url = pageURL;
  const reportStr = JSON.stringify(report, null, 2);
  system.stdout.write(reportStr);
  phantom.exit(0);
  exiting = true;
}

/**
 * Format/print error as JSON and exit PhantomJS with exit status 1
 */
function printErrorAndExit(errName, errMessage) {
  if (exiting) return;
  var errorStr = JSON.stringify({
    error: {
      name: errName,
      message: errMessage,
    },
  }, null, 2);
  system.stdout.write(errorStr);
  phantom.exit(1);
  exiting = true;
}

/**
 * Match page URL ?
 */
function matchPageURL(url) {
  return url === pageURL || url === pageURL+'/';
}

/**
 * Take a screenshot (PNG) and store it on report as base64 string
 */
function takeScreenshot() {
  debug('Taking screenshot...');
  report.screenshot = page.renderBase64('PNG');
}
