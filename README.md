# EcoMeter  (codename)

A web-based tool for assessing websites' ecodesign maturity.

Loads websites, compute metrics (from network activity, loaded payloads and the web page), and use them to assess the website's ecodesign maturity based on a list of best practices.
Eventually, answering additional questions about those things that cannot be automatically evaluated will help getting a more accurate vision of the ecodesign maturity level.

## Getting Started

The tool is composed of a web server written in Golang, storing data in a PostgreSQL database.
Client assets are preprocessed using Node.js.
Docker and docker-compose are used to run external dependencies during development (PostgreSQL, and SlimerJS, used to load the websites being assessed).

Following ecodesign best practices, everything that can be done at build time shouldn't be done at runtime.

### Prerequisites

 1. [Install Golang](https://golang.org/doc/install), setup your [`GOPATH` environment variable](https://golang.org/cmd/go/#hdr-GOPATH_environment_variable), and make sure `$GOPATH/bin` is in your `PATH`.
 2. [Install Node.js](https://nodejs.org) (and NPM)
 3. [Install Docker](https://docs.docker.com/engine/installation/) and [Docker-Compose](https://docs.docker.com/compose/install/)

Note: on Ubuntu Linux, you can use [Ubuntu Make](https://github.com/ubuntu/ubuntu-make#readme) to easily install (and update!) Golang and Node.js.

### Scripts

The project tries to follow GitHub's “scripts to rule them all” approach, trying to automate everything that can be automated.

 1. `script/bootstrap` checks for prerequisites and installs dependencies (run once after checkout).
 2. `script/update` updates dependencies (run after every `git pull`)
 3. `script/setup` initializes the PostgreSQL database.
 4. `script/server` launches the web server
 5. `script/lint` runs linting tasks on sources
 6. `script/test` runs automated tests; pass the `-short` flag to skip long-running tests that actually analyze pages;
    pass `-run TestAnalyze -httptest.serve=:9000` to start the testing server (here on port 9000), useful to debug the analysis process (e.g. with `script/report`);
    pass `-tags database` to enable database tests (this requires a running database, the connection string can be passed with environment variables or with `-dsn`)
 7. `script/cibuild` is meant for the continuous-integration server and runs all automated checks (lints and tests)
 8. `script/database` opens a PSQL client connected to the PostgreSQL container
 9. `script/build` builds the project and creates a deployable archive
 10. `script/report` generate a JSON report for the given URL (used for testing)

## Installation

TBD.
Runtime dependencies include SlimerJS and PostgreSQL.
The general idea is that it should be enough to expand an archive and run the binary (provided PostgreSQL runs on localhost, on standard port).

## Licensing

EcoMeter is licensed under AGPLv3.

    Copyright (C) 2016  The EcoMeter authors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Full text of the license can be found in the `LICENSE` file.
Incorporated libraries are published under their original licences.

The ecodesign best practices are licensed under Creative Commons Attribution-NonCommercial 4.0 International ([CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed)).

## Support

EcoMeter is an original work from (in alphabetical order) Atol Conseils & Développements, Bourgogne Numérique, Cadoles, GreenIT.fr, Logomotion, Planet Bourgogne, and Pulsar DS;
on an original idea of Frédéric Bordage (GreenIT.fr) and based on his own list of best practices built through years of training and consulting on software ecodesign.
It is funded by those organizations, and through a grant from the French Environment & Energy Management Agency (ADEME).
