import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';

export default {
  plugins: [
    nodeResolve({ main: true, jsnext: true }),
    commonjs(),
  ],
  input: 'webpage-analyzer/index.js',
  // PhantomJS core modules
  external: [ 'system', 'fs', 'child_process', 'webpage', 'webserver' ],
  output: {
    file: 'webpage-analyzer.js',
    format: 'cjs',
  },
};
