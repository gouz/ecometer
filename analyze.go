package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"time"
)

const (
	statusOK  status = "ok"
	statusNA  status = "na"
	statusNOK status = "nok"
)

type status string

type analyzer struct {
	slimerjs string
	script   string
}

type reportError struct {
	Name    string `json:"name"`
	Message string `json:"message"`
}

type ruleReport struct {
	ID     uint                   `json:"id"`
	Slug   string                 `json:"slug"`
	Status status                 `json:"status"`
	Data   map[string]interface{} `json:"data"`
}

type report struct {
	// Either Error is nil and the rest is filled,
	// or it's not and the rest is empty.

	Error *reportError `json:"error,omitempty"`

	Screenshot string       `json:"screenshot,omitempty"`
	Rules      []ruleReport `json:"rules,omitempty"`
}

func (a *analyzer) Analyze(url string) (*report, interface{}, error) {
	ctx, done := context.WithTimeout(context.Background(), 5*time.Minute)
	defer done()

	cmd := exec.CommandContext(ctx, a.slimerjs, a.script, url)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	if err = cmd.Start(); err != nil {
		return &report{
			Error: &reportError{
				Name:    "COMMAND_START_ERROR",
				Message: err.Error(),
			},
		}, nil, nil
	}

	var r report
	if err = json.NewDecoder(stdout).Decode(&r); err != nil {
		r = report{
			Error: &reportError{
				Name:    "INVALID_JSON",
				Message: err.Error(),
			},
		}
	}

	if err = cmd.Wait(); err != nil {
		if _, ok := err.(*exec.ExitError); !ok {
			return nil, cmd.ProcessState.SysUsage(), err
		}
	}

	return &r, cmd.ProcessState.SysUsage(), nil
}

func (r *report) findRuleByID(ruleID uint) (ruleReport, error) {
	for _, rr := range r.Rules {
		if rr.ID == ruleID {
			return rr, nil
		}
	}
	return ruleReport{}, fmt.Errorf("Cannot find result for rule '%v'", ruleID)
}
