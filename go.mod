module gitlab.com/ecoconceptionweb/ecometer

go 1.13

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/cortesi/modd v0.0.0-20191202231957-98a770274f90 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday v1.5.2
	github.com/shopspring/decimal v0.0.0-20200105231215-408a2507e114 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	go.ltgt.net v0.0.0-20170105144013-afb78d54dc7a
	golang.org/x/crypto v0.0.0-20200208060501-ecb85df21340 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
