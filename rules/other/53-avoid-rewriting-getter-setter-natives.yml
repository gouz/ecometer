id: 53
bookId: 67
title: Avoid rewriting getter/setter natives
slug: avoid-rewriting-getter-setter-natives
difficulty: easy
environmentalImpact: average
summary: | 
    Most object languages suggest standard getters and setters that the developer does not need to write.  
    Overloading them can increase the run and compilation time for these methods, which are generally better optimized by the language than the developer.  
    Consequently, use the standard getters and setters whenever possible, and implement methods that suit the purpose. This method has the added benefit of making maintenance easier for other developers, who will be more used to the behavior of standard getters/setters than those specifically implemented for the project.
example: |
    Favor the object-oriented approach without getters/setters but with a 'native' set for the private cheese property using the putCheese  class method:
    ```php
    class Fridge
    {
      private int cheese;
      void putCheese(int _number) { cheese += _number; }
    }

    void go_shopping(Fridge fridge)
    {
        fridge.putCheese(5);
    }
    ```
bookContext: Server Code
application: Application server
priority: 1
context: Development
