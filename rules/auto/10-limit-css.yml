id: 10
bookId: 25
title: Minimize the number of CSS files and compress them
slug: limit-css
difficulty: easy
environmentalImpact: high
summary: |
    Minimize the number of CSS files to reduce the number of HTTP requests. If several style sheets are used on all of the website's pages, concatenate them into one single file.

    Some CMS and frameworks offer ways to do such optimization automatically. The HTTP server can also be configured to compress and reduce the size of style sheets (see [best practice #79](http-compress.html)).
example: |
    With the Apache web server, simply add the following line in the *.htaccess* configuration file:
    ```apache
    # compress css:
    AddOutputFilterByType DEFLATE text/css
    ```
    This instruction activates the Deflate mode which compresses all the style sheets between the server and the HTTP client.

    Learn more about Deflate at:
    <http://httpd.apache.org/docs/2.4/mod/mod_deflate.html>
template: |
    You should limit the number of external CSS files to {{.goal}} per frame at most. This is the styleheets we've detected on this page:
    <ul>
    {{range $frameIndex, $frame := .frames}}
      {{$frameStylesheets := len .stylesheets}}
      {{if gt $frameStylesheets 0}}
      <li>
        <code title="{{$frame.url}}">{{ellipsis $frame.url 50}}</code> - {{$frameStylesheets}} stylesheets
      </li>
      {{end}}
    {{end}}
    </ul>
bookContext: Web template
application: CSS
priority: 0
context: Development
