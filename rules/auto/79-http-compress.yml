id: 79
bookId: 83
title: Compress the HTML output
slug: http-compress
difficulty: easy
environmentalImpact: high
summary: |
    You can compress the content of HTML pages to minimize bandwidth consumption between the client and the server.
    All modern browsers (for smartphones, tablets, notebook and desktop computers) accept HTML compressed via gzip or Deflate.
    The easiest way to do so is to configure the web server so that it compresses the HTML data stream, either on-the-fly or automatically, as it leaves the server.
    This practice (on-the-fly compression) is only beneficial for a HTML data stream as it is constantly evolving. When possible, we recommend that you manually compress static resources (e.g. CSS and JavaScript libraries) all in one go.
example: |
    With Apache, the Deflate and gzip compression methods offer considerable savings.
    A typical 26 KB HTML file is reduced to 6 KB after being compressed with gzip.

    Here is an example of an Apache configuration using the Deflate method:
    ```
    <IfModule mod_deflate.c>
    SetOutputFilter DEFLATE
    # Don’t compress
    SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|png)$ no-gzip dont-vary
    SetEnvIfNoCase Request_URI \.(?:exe|t?gz|zip|bz2|sit|rar)$ no-gzip dont-vary
    </IfModule>
    ```
template: |
    Received {{.compressedTextResources}} compressed text resources,
    on a total of {{.totalReceivedTextResources}} text resources worth compressing.
bookContext: Hosting
application: Resources and content
priority: 0
context: Hosting
