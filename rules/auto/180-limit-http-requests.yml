id: 180
bookId: 9
title: Limit the number of HTTP requests
slug: limit-http-requests
difficulty: medium
environmentalImpact: high
summary: |
    A page's download time client-side directly correlates to the number and size of files the browser has to download.

    For each file, the browser sends a GET HTTP to the server. It waits for the response, and then downloads the resource as soon as it is available. Depending on the type of web server you use, the more requests per page there are, the fewer pages the server can handle. Reducing the number of requests per page is key to reducing the number of HTTP severs needed to run the website, and consequently its environmental impact.

    There are several ways to reduce the number of requests per page:
    - Combine static files e.g. CSS and JavaScript libraries (see best practice #81)
    - Use a CSS sprite (see best practice #23) to group the interface's images
    - Favor glyphs over images (see best practice #18) and, in general, vectors over bitmaps.
    - Fill the browser cache as much as possible.
    <!-- TODO: Add links to best practices when implemented -->
example: |
    Potential saving: reduced server load, thus minimizing the economical and environmental footprint by reducing the amount of equipment needed (from HTTP servers to application servers and RDBMS).
template: |
    {{.totalRequests}} requests were made to display this page. In 2016, websites used {{.totalRequestsAverage2016}} requests on average (see [Internet Archive](http://httparchive.org/trends.php#bytesTotal&reqTotal)). We advise to aim for {{.ratio}}% of this value, in other words, {{.goal}} requests.
bookContext: Design
application: Technical
priority: 0
context: Design
