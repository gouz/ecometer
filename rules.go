package main

import (
	"bytes"
	"html/template"
	"math/rand"
)

type ruleContext string

const (
	contextDesign      ruleContext = "Design"
	contextDevelopment ruleContext = "Development"
	contextHosting     ruleContext = "Hosting"
)

type rule struct {
	ID      uint
	Slug    string
	Title   string
	Context ruleContext
	Summary template.HTML
}

func (r *rule) Template(data interface{}) (template.HTML, error) {
	var buf bytes.Buffer
	if err := views.ExecuteTemplate(&buf, r.Slug, data); err != nil {
		return "", err
	}
	return template.HTML(buf.String()), nil // #nosec
}

func randomRule() *rule {
	return &rules[rand.Intn(len(rules))]
}

// randomRules returns 2 rules at random that are guaranteed to be different
// from one another.
func randomRules() []*rule {
	var rules []*rule
	r1 := randomRule()
	rules = append(rules, r1)
	for len(rules) < 2 {
		r2 := randomRule()
		if r2.ID != r1.ID {
			rules = append(rules, r2)
		}
	}
	return rules
}
